# Noise detection results

## Description

Project that gathers all the results and simulations obtained for the detection of noise in computer graphics.

## Projects results

- [NoiseDetection-CNN](NoiseDetection-CNN/RESULTS.md)
- [NoiseDetection-attributes](NoiseDetection-attributes/RESULTS.md)
- [NoiseDetection-26-attributes](NoiseDetection-26-attributes/RESULTS.md)

## License

[The MIT License](LICENSE)